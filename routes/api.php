<?php

use Illuminate\Http\Request;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'UserController@login')->name('login');
Route::post('register', 'UserController@register')->name('register');


//User
Route::get('users', 'UserController@index');
Route::get('userssearch', 'UserController@search');
Route::get('users/{id}', 'UserController@show');
Route::post('users/{id}', 'UserController@update');
Route::post('users-delete/{id}', 'UserController@delete');

// Product
Route::get('products', 'ProductController@index');
Route::get('products/{id}', 'ProductController@show');
Route::get('productssearch', 'ProductController@search');
Route::post('products', 'ProductController@store');
Route::post('products/{id}', 'ProductController@update');
Route::post('products-delete/{id}', 'ProductController@delete');

// Buyer
Route::get('buyers', 'BuyerController@index');
Route::get('buyers/{id}', 'BuyerController@show');
Route::get('buyers-favorites/{id}', 'BuyerController@getFavorites');
Route::post('buyers', 'BuyerController@store');
Route::post('buyers/{id}', 'BuyerController@update');
Route::post('buyers-delete/{id}', 'BuyerController@delete');

// Category
Route::get('category', 'CategoryController@index');
Route::get('category-products', 'CategoryController@categoryProductsIndex');
Route::get('category/{id}', 'CategoryController@show');
Route::get('category-products/{id}', 'CategoryController@categoryProductsShow');
Route::post('category', 'CategoryController@store');
Route::post('category/{id}', 'CategoryController@update');
Route::post('category-delete/{id}', 'CategoryController@delete');

// DriverDetail
Route::get('driverdetail', 'DriverDetailController@index');
Route::get('driverdetail/{id}', 'DriverDetailController@show');
Route::post('driverdetail', 'DriverDetailController@store');
Route::post('driverdetail/{id}', 'DriverDetailController@update');
Route::post('driverdetail-delete/{id}', 'DriverDetailController@delete');

// Employee
Route::get('employees', 'EmployeeController@index');
Route::get('employees/{id}', 'EmployeeController@show');
Route::post('employees', 'EmployeeController@store');
Route::post('employees/{id}', 'EmployeeController@update');
Route::post('employees-delete/{id}', 'EmployeeController@delete');

//File
Route::get('file', 'FileController@index');
Route::get('file/{id}', 'FileController@show');
Route::post('file', 'FileController@storeFile');
Route::post('file/{id}', 'FileController@update');
Route::post('file-delete/{id}', 'FileController@removeFile');

// OrderDetail
Route::get('favorites', 'FavoriteController@index');
Route::get('favorites/{id}', 'FavoriteController@show');
Route::post('favorites', 'FavoriteController@store');
Route::post('favorites/{id}', 'FavoriteController@update');
Route::post('favorites-delete/{id}', 'FavoriteController@delete');

// FeedbackAndRate
Route::get('feedbackandrates', 'FeedbackAndRateController@index');
Route::get('feedbackandrates/{id}', 'FeedbackAndRateController@show');
Route::post('feedbackandrates', 'FeedbackAndRateController@store');
Route::post('feedbackandrates/{id}', 'FeedbackAndRateController@update');
Route::post('feedbackandrates-delete/{id}', 'FeedbackAndRateController@delete');

// OrderDetail
Route::get('orderdetails', 'OrderDetailController@index');
Route::get('orderdetails/{id}', 'OrderDetailController@show');
Route::post('orderdetails', 'OrderDetailController@store');
Route::post('orderdetails/{id}', 'OrderDetailController@update');
Route::post('orderdetails-delete/{id}', 'OrderDetailController@delete');

// OrderMasterlist
Route::get('ordermasterlists', 'OrderMasterlistController@index');
Route::get('ordermasterlists-runner-order', 'OrderMasterlistController@runnerOrder');
Route::get('ordermasterlists-runner-progress/{id}', 'OrderMasterlistController@runnerProgress');
Route::get('ordermasterlists-runner-history/{id}', 'OrderMasterlistController@runnerHistory');
Route::get('ordermasterlists/{id}', 'OrderMasterlistController@show');
Route::get('ordermasterlists-buyer/{id}', 'OrderMasterlistController@buyerOrders');
Route::get('ordermasterlists-employee/{id}', 'OrderMasterlistController@employeeOrder');
Route::post('ordermasterlists', 'OrderMasterlistController@store');
Route::post('ordermasterlists/{id}', 'OrderMasterlistController@update');
Route::post('ordermasterlists-delete/{id}', 'OrderMasterlistController@delete');

// OrderTrackingStatus
Route::get('ordertrackingstatuses', 'OrderTrackingStatusController@index');
Route::get('ordertrackingstatuses/{id}', 'OrderTrackingStatusController@show');
Route::post('ordertrackingstatuses', 'OrderTrackingStatusController@store');
Route::post('ordertrackingstatuses/{id}', 'OrderTrackingStatusController@update');
Route::post('ordertrackingstatuses-delete/{id}', 'OrderTrackingStatusController@delete');

// RequestDetail
Route::get('requestdetails', 'RequestDetailController@index');
Route::get('requestdetails/{id}', 'RequestDetailController@show');
Route::get('requestdetails-request-buyer/{id}', 'RequestDetailController@requestBuyer');
Route::post('requestdetails', 'RequestDetailController@store');
Route::post('requestdetails/{id}', 'RequestDetailController@update');
Route::post('requestdetails-delete/{id}', 'RequestDetailController@delete');

// RunnerTransaction
Route::get('runnertransactions', 'RunnerTransactionController@index');
Route::get('runnertransactions/{id}', 'RunnerTransactionController@show');
Route::post('runnertransactions', 'RunnerTransactionController@store');
Route::post('runnertransactions/{id}', 'RunnerTransactionController@update');
Route::post('runnertransactions-delete/{id}', 'RunnerTransactionController@delete');

// Stocks
Route::get('stocks', 'StocksController@index');
Route::post('stocks', 'StocksController@store');

Route::post('search', 'SearchController@search');

// Promo Code
Route::get('promocodes', 'PromoCodeController@index');
Route::get('promocodes/{id}', 'PromoCodeController@show');
Route::get('promocodesearch/{id}', 'PromoCodeController@search');
Route::post('promocodes', 'PromoCodeController@store');
Route::post('promocodes/{id}', 'PromoCodeController@update');

// supplier
Route::get('supplier', 'SuppliersController@index');
Route::get('supplier/{id}', 'SuppliersController@show');
Route::get('suppliersearch/{id}', 'SuppliersController@search');
Route::post('supplier', 'SuppliersController@store');
Route::post('supplier/{id}', 'SuppliersController@update');

// Returned Items
Route::get('returnitems', 'ReturnedItemsController@index');
Route::get('returnitems/{id}', 'ReturnedItemsController@show');
Route::get('returnitemsearch/{id}', 'ReturnedItemsController@search');
Route::post('returnitems', 'ReturnedItemsController@store');
Route::post('returnitems/{id}', 'ReturnedItemsController@update');
