<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

    	foreach (range(1,20) as $index) {
	        \App\Product::create([
	            'ProdName' => $faker->text($maxNbChars = 20),
	            'ProdDesc' => $faker->sentence,
	            'ProdSize' => $faker->sentence,
	            'ProdPieces' => $faker->sentence,
                'ProdPrice' => $faker->randomFloat($nbMaxDecimals = 1000, $min = 0, $max = 1000),
                'ProdQuantity' => $faker->randomDigit,
                'ProdReorderLevel' => $faker->randomDigit,
                'ProdExpirationDate' => $faker->randomDigit,
                'ProdReplenishDate' => $faker->randomDigit,
                'ProdMeasurement' => $faker->bothify($string = '## ml'),
                'ProdSupplier' => $faker->bothify($string = '## supplier'),
                'ProdPhoto' => 1,
                'category_id' => $faker->numberBetween(1,20),
                'enabled' => $faker->boolean
	        ]);
	    }
    }
}
