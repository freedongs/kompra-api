<?php

use Illuminate\Database\Seeder;

class PromoCodeSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

    	foreach (range(1,20) as $index) {
	        \App\PromoCodes::create([
                'code' => $faker->randomFloat($nbMaxDecimals = 1000, $min = 0, $max = 1000),
                'percent' => $faker->randomFloat($nbMaxDecimals = 1000, $min = 0, $max = 1000),
	        ]);
	    }
    }
}
