<?php

use Illuminate\Database\Seeder;

class StocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

    	foreach (range(1,20) as $index) {
	        '\App\Stocks'::create([
	            'ProdID' => $faker->numberBetween(1,20),
                'StockPrice' => $faker->randomFloat($nbMaxDecimals = 1000, $min = 0, $max = 1000),
                'StockQuantity' => $faker->randomDigit,
                'StockSupplier' => $faker->lastName,
	        ]);
	    }
    }
}
