<?php

use Illuminate\Database\Seeder;

class OrderTrackingStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $orderStatus = ['Pending', 'Processing', 'On the Way', 'Received'];

    	foreach ($orderStatus as $status) {
            $orderTrackingStatus = 'App\OrderTrackingStatus';

            $orderTrackingStatus::create([
                'OrderStatus' => $status,
            ]);
        }
    }
}
