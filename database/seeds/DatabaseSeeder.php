<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(DriverDetailSeeder::class);
        $this->call(FeedbackAndRateSeeder::class);
        $this->call(OrderDetailSeeder::class);
        $this->call(OrderMasterlistSeeder::class);
        $this->call(OrderTrackingStatusSeeder::class);
        $this->call(RequestDetailSeeder::class);
        $this->call(RunnerTransactionSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(StocksSeeder::class);
        $this->call(FavoriteTableSeeder::class);
    }
}
