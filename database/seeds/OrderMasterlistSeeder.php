<?php

use Illuminate\Database\Seeder;

class OrderMasterlistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $payment = ['COD', 'Card Type'];

    	foreach (range(1,20) as $index) {
            $orderMasterlist = 'App\OrderMasterlist';

            $temp = $orderMasterlist::create([
                'TotAmount' => $faker->randomFloat($nbMaxDecimals = 1000, $min = 0, $max = 1000),
                'PaymentType' => array_random($payment),
                'DeliveryDate' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'ShippingAdd' => $faker->address,
                'OrDetID' => $faker->numberBetween(1,20),
                'BUYERID' => $faker->numberBetween(1,6),
                'OrTracID' => $faker->numberBetween(1,4),
                'EMPID' => $faker->numberBetween(1,6),
                'ReqDetID' => $faker->numberBetween(1,20),
                'FEEDRATEID' => $faker->numberBetween(1,20)
            ]);
        }
    }
}
