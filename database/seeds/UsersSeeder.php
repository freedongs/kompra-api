<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

    	foreach (range(1,6) as $index) {
            $user = 'App\User';

            $firstName = $faker->firstName($gender = 'male'|'female');
            $lastName = $faker->lastName;
            $type = array('buyer', 'employee');

            $temp = $user::create([
	            'name' => $firstName.' '.$lastName,
	            'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('password'),
                'EMPID' => $index,
                'BUYERID' => $index,
                'user_type' => array_random($type),
                'enabled' => "1",
                'pwd' => "0",
            ]);
            
            $temp->employee()->create([
                'EMPID' => $index,
                'EMPFname' => $firstName,
                'EMPLname' => $lastName,
                'EMPAdd' => $faker->address, 
                'EMPConNum' => $faker->tollFreePhoneNumber, 
                'EMPAge' => $faker->randomDigitNotNull, 
                'EMPPosi' => str_random(10).'69',
                'EMPDoB' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'DriverID' => $temp->id
            ]);

            $temp->buyer()->create([
                'BUYERID' => $index,
                'BuyFname' => $firstName,
                'BuyMI' => $faker->randomLetter, 
                'BuyLname' => $lastName, 
                'BuyAge' => $faker->randomDigitNotNull,
                'BuyConNum' => $faker->tollFreePhoneNumber,
                'BuyGender' => 'Female', 
                'BuyOccu' => $faker->jobTitle, 
                'BuyAdd' => $faker->address,
                'BuyDoB' => $faker->date($format = 'Y-m-d', $max = 'now'), 
            ]);
        }
    }
}
