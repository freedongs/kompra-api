<?php

use Illuminate\Database\Seeder;

class ReturnedItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

        foreach (range(1,20) as $index) {
            \App\ReturnedItems::create([
                'ProdQty' => $faker->numberBetween(1,10),
                'id' => $faker->numberBetween(1,10),
                'received' => '0'
            ]);
        }
    }
}
