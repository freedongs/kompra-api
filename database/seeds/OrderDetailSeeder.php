<?php

use Illuminate\Database\Seeder;

class OrderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

    	foreach (range(1,20) as $index) {
            $orderDetail = 'App\OrderDetail';

            $temp = $orderDetail::create([
                'OrDetID' => $faker->numberBetween(1,6),
                'OrDetCount' => $faker->randomDigitNotNull,
                'OrDetItemDiscount' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 0.100),
                'OrDetDate' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'ProdID' => $faker->numberBetween(1,20)
            ]);
        }
    }
}
