<?php

use Illuminate\Database\Seeder;

class DriverDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\lv_LV\Person($faker));

    	foreach (range(1,6) as $index) {
            $driverDetail = 'App\DriverDetail';

            $temp = $driverDetail::create([
                'DriverID' => $index,
                'DriverLicenseNum' => $faker->personalIdentityNumber, 
                'EMPID' => $index
            ]);
        }
    }
}
