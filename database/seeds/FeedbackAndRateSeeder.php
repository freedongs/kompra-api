<?php

use Illuminate\Database\Seeder;

class FeedbackAndRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

    	foreach (range(1,20) as $index) {
            $feedbackAndRate = 'App\FeedbackAndRate';

            $temp = $feedbackAndRate::create([
                'ORDERRATE' => 1.5 ,
                'ORDERFEEDBACKS' => $faker->text
            ]);
        }
    }
}
