<?php

use Illuminate\Database\Seeder;

class RequestDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

    	foreach (range(1,20) as $index) {
            $requestDetail = 'App\RequestDetail';

            $temp = $requestDetail::create([
                'ItemReqName' => $faker->text($maxNbChars = 20), 
                'ItemReqDesc' => $faker->sentence, 
                'ItemReqPrice' => $faker->randomFloat($nbMaxDecimals = 1000, $min = 0, $max = 1000),
                'approve' => $faker->boolean,
                'ItemReqQuantity' => $faker->randomDigit,
                'BUYERID' => $faker->numberBetween(1,20)
            ]);
        }
    }
}
