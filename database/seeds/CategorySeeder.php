<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

        foreach (range(1,20) as $index) {
            $categories = 'App\Category';

            $temp = $categories::create([
                'categoryName' => $faker->text($maxNbChars = 20),
                'categoryPhoto' => 1,
                'enabled' => 0,
            ]);
        }
    }
}
