<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackAndRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_and_rates', function (Blueprint $table) {
            $table->increments('FEEDRATEID');
            $table->float('ORDERRATE');
            $table->string('ORDERFEEDBACKS', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('feedback_and_rates');
        Schema::enableForeignKeyConstraints();
    }
}
