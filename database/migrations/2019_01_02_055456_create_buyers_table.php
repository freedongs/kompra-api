<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyers', function (Blueprint $table) {
            $table->integer('BUYERID');
            $table->string('BuyFname', 50);
            $table->string('BuyMI', 50)->nullable();
            $table->string('BuyLname', 50);
            $table->integer('BuyAge');
            $table->string('BuyConNum');
            $table->string('BuyGender', 10);
            $table->string('BuyOccu', 50);
            $table->string('BuyAdd', 255);
            $table->date('BuyDoB');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('buyers');
        Schema::enableForeignKeyConstraints();
    }
}
