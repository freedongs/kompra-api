<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_details', function (Blueprint $table) {
            $table->integer('DriverID')->nullable();
            $table->string('DriverLicenseNum', 20);
            $table->integer('EMPID')->unsigned()->nullable();
            $table->timestamps();
        });

        // Schema::table('buyers', function(Blueprint $table) {
        //     $table->foreign('OrMasID')->references('OrMasID')->on('order_masterlists')->onDelete('cascade');
        // });

        // Schema::table('feedback_and_rates', function(Blueprint $table) {
        //     $table->foreign('OrMasID')->references('OrMasID')->on('order_masterlists')->onDelete('cascade');            
        // });

        // Schema::table('order_tracking_statuses', function(Blueprint $table) {
        //     $table->foreign('OrMasID')->references('OrMasID')->on('order_masterlists')->onDelete('cascade');
        // });

        // Schema::table('request_details', function(Blueprint $table) {
        //     $table->foreign('OrMasID')->references('OrMasID')->on('order_masterlists')->onDelete('cascade');            
        // });

        // Schema::table('runner_transactions', function(Blueprint $table) {
        //     $table->foreign('EMPID')->references('EMPID')->on('employees')->onDelete('cascade');
        //     $table->foreign('ReqDetID')->references('ReqDetID')->on('request_details')->onDelete('cascade');
        // });

        // Schema::table('order_masterlists', function(Blueprint $table) {
        //     $table->foreign('OrDetID')->references('OrDetID')->on('order_details')->onDelete('cascade');
        //     $table->foreign('FEEDRATEID')->references('FEEDRATEID')->on('feedback_and_rates')->onDelete('cascade');
        //     $table->foreign('OrTracID')->references('OrTracID')->on('order_tracking_statuses')->onDelete('cascade');
        //     $table->foreign('DriverID')->references('DriverID')->on('driver_details')->onDelete('cascade');
        //     $table->foreign('ReqDetID')->references('ReqDetID')->on('request_details')->onDelete('cascade');
        //     $table->foreign('BUYERID')->references('BUYERID')->on('buyers')->onDelete('cascade');
        // });

        // Schema::table('order_details', function(Blueprint $table) {
        //     $table->foreign('ProdID')->references('ProdID')->on('products')->onDelete('cascade');
        // });

        // Schema::table('employees', function(Blueprint $table) {
        //     $table->foreign('RunnerTransID')->references('RunnerTransID')->on('runner_transactions')->onDelete('cascade');            
        //     $table->foreign('DriverID')->references('DriverID')->on('driver_details')->onDelete('cascade');            
        // });

        // Schema::table('driver_details', function(Blueprint $table) {
        //     $table->foreign('OrMasID')->references('OrMasID')->on('order_masterlists')->onDelete('cascade');
        //     $table->foreign('EMPID')->references('EMPID')->on('employees')->onDelete('cascade');
        // });

        // Schema::table('users', function(Blueprint $table) {
        //     $table->foreign('EMPID')->references('EMPID')->on('employees')->onDelete('cascade');
        //     $table->foreign('BUYERID')->references('BUYERID')->on('buyers')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('driver_details');
        Schema::enableForeignKeyConstraints();
    }
}
