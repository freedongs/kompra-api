<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('ProdID');
            $table->string('ProdName', 50);
            $table->string('ProdDesc', 255);
            $table->string('ProdSize', 255);
            $table->string('ProdPieces', 255);
            $table->double('ProdPrice', 15, 8);
            $table->integer('ProdQuantity');
            $table->integer('ProdReorderLevel');
            $table->string('ProdExpirationDate', 255);
            $table->string('ProdReplenishDate', 255);
            $table->string('ProdMeasurement', 50);
            $table->string('ProdSupplier', 50);
            $table->integer('ProdPhoto')->nullable();
            $table->integer('category_id');
            $table->boolean('enabled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('products');
        Schema::enableForeignKeyConstraints();
    }
}
