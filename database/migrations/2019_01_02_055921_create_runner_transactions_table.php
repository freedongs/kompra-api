<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRunnerTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runner_transactions', function (Blueprint $table) {
            $table->increments('RunnerTransID');
            $table->integer('EMPID')->unsigned()->nullable();
            $table->integer('OrMasID')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('runner_transactions');
        Schema::enableForeignKeyConstraints();
    }
}
