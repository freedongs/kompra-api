<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_details', function (Blueprint $table) {
            $table->increments('ReqDetID');
            $table->string('ItemReqName', 50);
            $table->string('ItemReqDesc', 250);
            $table->double('ItemReqPrice', 15, 8);
            $table->integer('ItemReqQuantity');
            $table->boolean('approve')->nullable();
            $table->integer('BUYERID')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('request_details');
        Schema::enableForeignKeyConstraints();
    }
}
