<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderMasterlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_masterlists', function (Blueprint $table) {
            $table->increments('OrMasID');
            $table->double('TotAmount', 15, 8);
            $table->string('PaymentType', 50);
            $table->timestamp('DeliveryDate')->nullable;
            $table->string('ShippingAdd', 255);
            $table->integer('OrDetID')->unsigned()->nullable();
            $table->integer('BUYERID')->unsigned()->nullable();
            $table->integer('FEEDRATEID')->unsigned()->nullable();
            $table->integer('OrTracID')->unsigned()->nullable();
            $table->integer('EMPID')->unsigned()->nullable();
            $table->integer('ReqDetID')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('order_masterlists');
        Schema::enableForeignKeyConstraints();
    }
}
