<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->integer('EMPID')->nullable();
            $table->string('EMPFname', 50);
            $table->string('EMPLname', 50);
            $table->string('EMPAdd', 255);
            $table->string('EMPConNum');
            $table->integer('EMPAge');
            $table->string('EMPPosi', 50);
            $table->date('EMPDoB');
            $table->integer('DriverID')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('employees');
        Schema::enableForeignKeyConstraints();
    }
}
