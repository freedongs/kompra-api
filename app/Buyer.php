<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    //
    protected $primaryKey = 'BUYERID';
    protected $fillable = ['BUYERID', 'BuyFname', 'BuyMI', 'BuyLname', 'BuyAge', 'BuyConNum', 'BuyGender', 'BuyOccu', 'BuyAdd', 'BuyDoB'];

    public function orderMasterlist() {

        return $this->hasMany('App\OrderMasterlist', 'OrMasID', 'OrMasID');
    }

    public function user() {

        return $this->hasOne('App\User', 'id', 'BUYERID');
    }

    public function favorite() {

    	return $this->hasMany('App\Favorite', 'BUYERID', 'BUYERID');
    }
}
