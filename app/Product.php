<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'ProdID';
    protected $fillable = ['ProdName', 'ProdReorderLevel', 'ProdExpirationDate', 'ProdReplenishDate', 'ProdSize', 'ProdPieces','ProdDesc', 'ProdPrice', 'ProdQuantity', 'ProdMeasurement', 'ProdSupplier', 'ProdPhoto', 'category_id','enabled'];

    public function orderDetails() {

        return $this->belongsTo('App\OrderDetail', 'ProdID', 'ProdID');
    }

    public function category() {

        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function favorite() {

        return $this->hasOne('App\Favorite', 'ProdID', 'ProdID');
    }

    public function file() {

        return $this->hasOne('App\File', 'id', 'ProdPhoto');
    }
}
