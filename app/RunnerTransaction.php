<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RunnerTransaction extends Model
{
    //
    protected $primaryKey = 'RunnerTransID';
    protected $fillable = ['EMPID'];

    public function employee() {

        return $this->belongsTo('App\Employee', 'EMPID', 'EMPID');
    }
}
