<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $primaryKey = 'EMPID';
    protected $fillable = ['EMPID', 'EMPFname', 'EMPLname', 'EMPAdd', 'EMPConNum', 'EMPAge', 'EMPPosi', 'EMPDoB', 'DriverID'];

    public function runnerTransaction() {

        return $this->hasMany('App\RunnerTransaction', 'EMPID', 'EMPID');
    }

    public function requestDetail() {

        return $this->hasMany('App\RequestDetail', 'EMPID', 'EMPID');
    }

    public function orderMasterlist() {

        return $this->hasMany('App\OrderMasterlist', 'DriverID', 'EMPID');
    }

    public function driverDetail() {

        return $this->hasOne('App\DriverDetail', 'DriverID', 'DriverID');
    }

    public function user() {

        return $this->hasOne('App\User', 'id', 'EMPID');
    }
}
