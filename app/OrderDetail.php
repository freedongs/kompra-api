<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    protected $primaryKey = 'OrDetID';
    protected $fillable = ['OrDetID', 'OrDetCount', 'OrDetItemDiscount', 'OrDetDate', 'ProdID', 'returned'];

    public function products() {

        return $this->hasOne('App\Product', 'ProdID', 'ProdID');
    }

    public function orderMasterlist() {

        return $this->belongsTo('App\OrderMasterlist', 'OrMasID', 'OrMasID');
    }
}
