<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Stocks extends Model
{
     use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'StockID';
    protected $fillable = ['ProdID', 'StockPrice', 'StockQuantity', 'StockSupplier'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['remember_token'
    ];

    public function products() {

        return $this->belongsTo('App\Product', 'ProdID', 'ProdID');
    }
}
