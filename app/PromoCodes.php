<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PromoCodes extends Model
{
	use HasApiTokens, Notifiable;

    //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'PromoID';
    protected $fillable = ['code', 'percent','enabled'];

}
