<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderMasterlist extends Model
{
    //
    protected $primaryKey = 'OrMasID';
    protected $fillable = ['TotAmount', 'PaymentType', 'DeliveryDate', 'ShippingAdd', 'OrDetID', 'BUYERID', 'OrTracID', 'EMPID', 'FEEDRATEID'];

    public function orderDetails() {

        return $this->hasMany('App\OrderDetail', 'OrDetID', 'OrDetID');
    }

    public function buyer() {

        return $this->belongsTo('App\Buyer', 'BUYERID', 'BUYERID');
    }

    public function feedbackRating() {

        return $this->belongsTo('App\FeedbackAndRate', 'FEEDRATEID', 'FEEDRATEID');
    }

    public function orderTrackingStatus() {

        return $this->hasOne('App\OrderTrackingStatus', 'OrTracID', 'OrTracID');
    }

    public function driverDetail() {

        return $this->hasMany('App\DriverDetail', 'DriverID', 'DriverID');
    }

    public function employee() {

        return $this->hasOne('App\Employee', 'EMPID', 'EMPID');
    }
}
