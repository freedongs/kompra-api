<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDetail extends Model
{
    //
    protected $primaryKey = 'ReqDetID';
    protected $fillable = ['ItemReqName', 'ItemReqDesc', 'ItemReqPrice', 'ItemReqQuantity', 'approve', 'BUYERID'];

    public function employee() {

        return $this->belongsTo('App\Employee', 'OrMasID', 'OrMasID');
    }

    public function runnerTransaction() {

        return $this->hasMany('App\RunnerTransaction', 'RunnerTransID', 'RunnerTransID');
    }

    public function buyers() {

        return $this->belongsTo('App\Buyer', 'BUYERID', 'BUYERID');
    }
}
