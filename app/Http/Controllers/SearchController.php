<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class SearchController extends Controller
{
    //
    public function search(Request $request) {
        $validator = Validator::make($request->all(), [
            'term' => 'required',
            'is_category' => 'boolean|required'
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        if ($request->is_category) {
            $this->result['data'] = Category::with('product.category', 'file')->has('product')->where('categoryName', 'like', '%' . $request->term . '%')->get();
        } else {
            $this->result['data'] = Product::with('category', 'file')->where('ProdName', 'like', '%' . $request->term . '%')->get();
        }
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
