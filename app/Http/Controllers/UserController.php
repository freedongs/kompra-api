<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\Employee;
use App\DriverDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;
    protected $result;

    //
    public function welcome(){
        return view('welcome');
    }
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $userInfo = User::with('employee.driverDetail', 'buyer')->find(Auth::id());
            $userInfo->enabled = $userInfo->enabled == 1 ? true : false;
            $userInfo->pwd = $userInfo->pwd == 1 ? true : false;
            $result['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['data' => $userInfo, 'success' => $result], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'address' => 'required',
            'age' => 'required|numeric',
            'occupation' => 'required',
            'gender' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'user_type' => 'required',
            'contactNumber' => 'required',
            'pwd' => 'required_if :user_type,buyer',
            'enabled' => 'required_if :user_type,buyer',
            'dob' => 'required',
            'c_password' => 'required|same:password',
            'license_number' => 'required_if :user_type,employee'
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $input = $request->all();
        $insert = new User;
        $insert->name = $input['firstName'].' '.$input['lastName'];
        $insert->email = $input['email'];
        $insert->password = bcrypt($input['password']);
        $insert->user_type = $input['user_type'];
        $insert->enabled = $input['enabled'] ? true : false;
        $insert->pwd = $input['pwd'] ? true : false;
        $insert->save();

        if($input['user_type'] == 'buyer'){

            User::find($insert->id)->update(['BUYERID' => $insert->id]);
            Buyer::create([
                'BUYERID' => $insert->id,
                'BuyFname' => $input['firstName'],
                'BuyLname' => $input['lastName'],
                'BuyAge' => $input['age'],
                'BuyGender' => $input['gender'],
                'BuyConNum' => $input['contactNumber'],
                'BuyOccu' => $input['occupation'],
                'BuyAdd' => $input['address'],
                'BuyDoB' => $input['dob'],
            ]);

        } elseif($input['user_type'] == 'employee'){
            User::find($insert->id)->update(['EMPID' => $insert->id]);
            Employee::create([
                'EMPID' => $insert->id,
                'EMPFname' => $input['firstName'],
                'EMPLname' => $input['lastName'],
                'EMPAdd' => $input['address'],
                'EMPConNum' => $input['contactNumber'],
                'EMPAge' => $input['age'],
                'EMPPosi' => $input['occupation'],
                'EMPDoB' => $input['dob'],
            ]);
            DriverDetail::create([
                'DriverID' =>  $insert->id,
                'DriverLicenseNum' => $input['license_number'],
                'EMPID' => $insert->id
            ]);
        }
        $this->result['data'] = $insert;
        $this->result['token'] =  $insert->createToken('MyApp')-> accessToken;
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function index() {

        $this->result['data'] = User::with('buyer', 'employee')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function search(Request $request) {
                $validator = Validator::make($request->all(), [
                    'key' => 'required'
                ]);
                try{
                    $this->result['data'] = User::where('name', 'like', '%' . $request->key . '%')
                    ->orWhere ('email', 'like', '%' . $request->key . '%')
                    ->orWhere ('user_type', 'like', '%' . $request->key . '%')
                    ->get();
                    $this->result['statusCode'] = 200;

                }catch (ModelNotFoundException $exception){
                    $this->result['error'] = $exception->getMessage();
                    $this->result['statusCode'] = 500;

                }

                return response()->json($this->result, $this->result['statusCode']);
    }


    public function show($id) {

        try{
            $this->result['data'] = User::with('buyer', 'employee')->where('id', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['statusCode'] = 500;
            $this->result['error'] = $exception->getMessage();

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $user = User::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'firstName' => 'required',
            'lastName' => 'required',
            'address' => 'required',
            'age' => 'required|numeric',
            'occupation' => 'required',
            'gender' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'user_type' => 'required',
            'dob' => 'required_if:user_type,buyer'
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $input = $request->all();
        $user->name = $input['firstName'].' '.$input['lastName'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->user_type = $input['user_type'];
        $user->enabled = $input['enabled'];
        $user->pwd = $input['pwd'];
        $user->save();

        if($input['user_type'] == 'buyer'){
            Buyer::find($id)->update([
                'BUYERID' => $id,
                'BuyFname' => $input['firstName'],
                'BuyLname' => $input['lastName'],
                'BuyAge' => $input['age'],
                'BuyGender' => $input['gender'],
                'BuyOccu' => $input['occupation'],
                'BuyAdd' => $input['address'],
                'BuyDoB' => $input['dob'],
            ]);

        } elseif($input['user_type'] == 'employee'){
            Employee::find($id)->update([
                'EMPID' => $id,
                'EMPFname' => $input['firstName'],
                'EMPLname' => $input['lastName'],
                'EMPAdd' => $input['address'],
                'EMPConNum' => $input['contactNumber'],
                'EMPAge' => $input['age'],
                'EMPPosi' => $input['occupation'],
            ]);
        }

        $this->result['data'] = $user->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $user = User::findOrFail($id);
            Buyer::where('BUYERID', $id)->delete();
            Employee::where('EMPID', $id)->delete();
        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $user->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
