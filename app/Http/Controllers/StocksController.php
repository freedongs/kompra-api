<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stocks;
use App\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class StocksController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = Stocks::with('products')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'ProdID' => 'required',
            'StockPrice' => 'required',
            'StockQuantity' => 'required',
            'StockSupplier' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = Stocks::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
