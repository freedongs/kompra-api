<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suppliers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class SuppliersController extends Controller
{
    //
    //
    protected $result;

    public function index() {

        $this->result['data'] = Suppliers::get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'supplier_name' => 'required',
            'supplier_address' => 'required',
            'supplier_contact' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = Suppliers::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function show($id) {

            try{
                $this->result['data'] = Suppliers::findOrFail($id);
                $this->result['statusCode'] = 200;

            }catch (ModelNotFoundException $exception){
                $this->result['error'] = $exception->getMessage();
                $this->result['statusCode'] = 500;

            }

            return response()->json($this->result, $this->result['statusCode']);
        }

         public function search($id) {

            try{
                $this->result['data'] = Suppliers::where('supplier_name',$id)->get();
                $this->result['statusCode'] = 200;

            }catch (ModelNotFoundException $exception){
                $this->result['error'] = $exception->getMessage();
                $this->result['statusCode'] = 500;

            }

            return response()->json($this->result, $this->result['statusCode']);
         }

        public function update(Request $request, $id) {

                try{
                    $promo_codes = Suppliers::findOrFail($id);

                } catch (ModelNotFoundException $exception){
                    $this->result['error'] = $exception->getMessage();
                    $this->result['statusCode'] = 500;

                    return response()->json($this->result, $this->result['statusCode']);
                }

                $validator = Validator::make($request->all(), [
                    'supplier_name' => 'required',
                    'supplier_address' => 'required',
                    'supplier_contact' => 'required'
                ]);

                if ($validator->fails()) {
                    $this->result['error'] = $validator->errors();
                    $this->result['statusCode'] = 401;

                    return response()->json($this->result, $this->result['statusCode']);
                }

                $input = $request->all();

                $this->result['data'] = $promo_codes->update($input);
                $this->result['statusCode'] = 200;

                return response()->json($this->result, $this->result['statusCode']);
        }
}
