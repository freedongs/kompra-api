<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    //
    public function storeFile(Request $request) {

        if ($request->hasFile('image')) {

            $filename = $request->image->hashName();
//            $filename = $request->image->getClientOriginalName();
            $filesize = $request->image->getClientSize();

            $request->image->storeAs('public/upload', $filename);

            $file = new File();

            $file->image_url = $filename;
            $file->size = $filesize;

            $file->save();

            return $file->id;
        } else {

            return NULL;
        }
    }

    public function removeFile(Request $request) {

        if ($request->hasFile('image')) {
            $file = File::find($request->ProdPhoto);
            $file->delete();
            Storage::delete('public/upload/'.$file->image_url);

            return true;
        } else {
            return false;
        }

    }
}
