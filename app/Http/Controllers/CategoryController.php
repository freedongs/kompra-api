<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class CategoryController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = Category::with('file')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function categoryProductsIndex() {

        $this->result['data'] = Category::with('product.category', 'product.file','file')->has('product')->where('enabled', '=', true)->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function show($id) {

        try{
            $this->result['data'] = Category::with('file')->findOrFail($id);
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function categoryProductsShow($id) {

        try{
            $this->result['data'] = Category::with('product.category', 'product.file','file')->where('id', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'categoryName' => 'required',
            'image' => 'required|image',
            'enabled' => 'required'
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }
        $file = new FileController();
        $request['categoryPhoto'] = $file->storeFile($request);

        $this->result['data'] = Category::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $categories = Category::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'categoryName' => 'required',
            'image' => 'required|image',
            'categoryPhoto' => 'required',
            'enabled' => 'required'
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }
        $file = new FileController();
        $file->removeFile($request);
        $request['categoryPhoto'] = $file->storeFile($request);
        $input = $request->all();

        $this->result['data'] = $categories->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $categories = Category::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $categories->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
