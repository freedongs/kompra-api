<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PromoCodes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class PromoCodeController extends Controller
{
    protected $result;

        public function index() {

            $this->result['data'] = PromoCodes::get();
            $this->result['statusCode'] = 200;

            return response()->json($this->result, $this->result['statusCode']);
        }

        public function store(Request $request) {

            $validator = Validator::make($request->all(), [
                'code' => 'required',
                'percent' => 'required',
            ]);

            if ($validator->fails()) {
                $this->result['error'] = $validator->errors();
                $this->result['statusCode'] = 401;

                return response()->json($this->result, $this->result['statusCode']);
            }

            $this->result['data'] = PromoCodes::create($request->all());
            $this->result['statusCode'] = 200;

            return response()->json($this->result, $this->result['statusCode']);
        }

        public function show($id) {

	        try{
	            $this->result['data'] = PromoCodes::findOrFail($id);
	            $this->result['statusCode'] = 200;

	        }catch (ModelNotFoundException $exception){
	            $this->result['error'] = $exception->getMessage();
	            $this->result['statusCode'] = 500;

	        }

	        return response()->json($this->result, $this->result['statusCode']);
    	}

         public function search($id) {

	        try{
	            $this->result['data'] = PromoCodes::where('code',$id)->get();
	            $this->result['statusCode'] = 200;

	        }catch (ModelNotFoundException $exception){
	            $this->result['error'] = $exception->getMessage();
	            $this->result['statusCode'] = 500;

	        }

	        return response()->json($this->result, $this->result['statusCode']);
         }

    	public function update(Request $request, $id) {

			    try{
			        $promo_codes = PromoCodes::findOrFail($id);

			    } catch (ModelNotFoundException $exception){
			        $this->result['error'] = $exception->getMessage();
			        $this->result['statusCode'] = 500;

			        return response()->json($this->result, $this->result['statusCode']);
			    }

			    $validator = Validator::make($request->all(), [
	                'code' => 'required',
	                'percent' => 'required',
	                'enabled' => 'required'
			    ]);

			    if ($validator->fails()) {
			        $this->result['error'] = $validator->errors();
			        $this->result['statusCode'] = 401;

			        return response()->json($this->result, $this->result['statusCode']);
			    }

			    $input = $request->all();

			    $this->result['data'] = $promo_codes->update($input);
			    $this->result['statusCode'] = 200;

			    return response()->json($this->result, $this->result['statusCode']);
    	}

}
