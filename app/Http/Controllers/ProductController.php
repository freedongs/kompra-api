<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class ProductController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = Product::with('category', 'file')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function search(Request $request) {
            $validator = Validator::make($request->all(), [
                'key' => 'required'
            ]);
            try{
                $this->result['data'] = Product::with('file','category')->leftJoin('categories', 'categories.id', '=', 'category_id')->where('ProdName', 'like', '%' . $request->key . '%')
                ->orWhere ('ProdDesc', 'like', '%' . $request->key . '%')
                ->orWhere ('ProdPrice', 'like', '%' . $request->key . '%')
                ->orWhere ('categories.categoryName', 'like', '%' . $request->key . '%')
                ->get();
                $this->result['statusCode'] = 200;

            }catch (ModelNotFoundException $exception){
                $this->result['error'] = $exception->getMessage();
                $this->result['statusCode'] = 500;

            }

            return response()->json($this->result, $this->result['statusCode']);
        }

    public function show($id) {
       try{
            $this->result['data'] = Product::with('file')->findOrFail($id);
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'ProdName' => 'required',
            'ProdDesc' => 'required',
            'ProdPrice' => 'required',
            'ProdQuantity' => 'required',
            'ProdMeasurement' => 'required',
            'ProdSupplier' => 'required',
            'ProdReorderLevel' => 'required',
            'ProdExpirationDate' => 'required',
            'ProdReplenishDate' => 'required',
            'ProdSize' => 'required',
            'ProdPieces' => 'required',
            'image' => 'image',
            'enabled' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }
        $file = new FileController();
        $request['ProdPhoto'] = $file->storeFile($request);

        $this->result['data'] = Product::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $product = Product::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'ProdName' => 'required',
            'ProdDesc' => 'required',
            'ProdPrice' => 'required',
            'ProdQuantity' => 'required',
            'ProdMeasurement' => 'required',
            'ProdSupplier' => 'required',
            'ProdReorderLevel' => 'required',
            'ProdExpirationDate' => 'required',
            'ProdReplenishDate' => 'required',
            'ProdSize' => 'required',
            'ProdPieces' => 'required',
            'ProdPhoto' => 'required',
            'image' => 'image',
            'enabled' => 'required'
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $file = new FileController();
        $file->removeFile($request);
        $request['ProdPhoto'] = $file->storeFile($request);
        $input = $request->all();

        $this->result['data'] = $product->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $product = Product::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $product->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
