<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorite;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class FavoriteController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = Favorite::all();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function show($id) {

        try{
            $this->result['data'] = Favorite::findOrFail($id);
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'BUYERID' => 'required',
            'ProdID' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = Favorite::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $favorite = Favorite::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'BUYERID' => 'required',
            'ProdID' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $input = $request->all();

        $this->result['data'] = $favorite->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $favorite = Favorite::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $favorite->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
