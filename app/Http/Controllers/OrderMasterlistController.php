<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderMasterlist;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class OrderMasterlistController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = OrderMasterlist::with('orderDetails.products.file', 'buyer', 'feedbackRating', 'employee.driverDetail', 'orderTrackingStatus')->has('orderDetails.products')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function runnerOrder() {

        $this->result['data'] = OrderMasterlist::with('orderDetails.products.file', 'buyer', 'feedbackRating', 'employee.driverDetail', 'orderTrackingStatus')->has('orderDetails.products')->whereNull('EMPID')->whereNull('OrTracID')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function runnerProgress($id) {

        $this->result['data'] = OrderMasterlist::with('orderDetails.products.file', 'buyer', 'feedbackRating', 'employee.driverDetail', 'orderTrackingStatus')->has('orderDetails.products')->where('EMPID', $id)->where('OrTracID', '!=' , 4)->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function runnerHistory($id) {

        $this->result['data'] = OrderMasterlist::with('orderDetails.products.file', 'buyer', 'feedbackRating', 'employee.driverDetail', 'orderTrackingStatus')->has('orderDetails.products')->where('EMPID', $id)->where('OrTracID', 4)->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function buyerOrders($id) {

        try{
            $this->result['data'] = OrderMasterlist::with('orderDetails.products.file', 'buyer', 'feedbackRating', 'employee.driverDetail', 'orderTrackingStatus')->has('orderDetails.products')->where('BUYERID', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function employeeOrder($id) {

        try{
            $this->result['data'] = OrderMasterlist::with('orderDetails.products.file', 'buyer', 'feedbackRating', 'employee.driverDetail', 'orderTrackingStatus')->has('orderDetails.products')->where('EMPID', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function show($id) {
        try{
            $this->result['data'] = OrderMasterlist::with('orderDetails.products.file','feedbackRating','buyer')->where('OrMasID', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'TotAmount' => 'required|numeric',
            'PaymentType' => 'required',
            'DeliveryDate' => 'required',
            'ShippingAdd' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = OrderMasterlist::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $orderMasterlist = OrderMasterlist::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'TotAmount' => 'required|numeric',
            'PaymentType' => 'required',
            'DeliveryDate' => 'required',
            'ShippingAdd' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $input = $request->all();

        $this->result['data'] = $orderMasterlist->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $orderMasterlist = OrderMasterlist::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $orderMasterlist->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
