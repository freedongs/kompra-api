<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class RequestDetailController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = RequestDetail::with('buyers')->get();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function show($id) {
        
        try{
            $this->result['data'] = RequestDetail::with('buyers')->where('ReqDetID', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function requestBuyer($id) {

        try{
            $this->result['data'] = RequestDetail::where('BUYERID', $id)->get();
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'ItemReqName' => 'required',
            'ItemReqDesc' => 'required',
            'ItemReqPrice' => 'required|numeric',
            'ItemReqQuantity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = RequestDetail::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $requestDetail = RequestDetail::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'OrderRefNum' => 'required',
            'ItemReqName' => 'required',
            'ItemReqDesc' => 'required',
            'ItemReqPrice' => 'required|numeric',
            'ItemReqQuantity' => 'required|numeric',
            'approve' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $input = $request->all();

        $this->result['data'] = $requestDetail->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $requestDetail = RequestDetail::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $requestDetail->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
