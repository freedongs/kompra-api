<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buyer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;

class BuyerController extends Controller
{
    //
    protected $result;

    public function index() {

        $this->result['data'] = Buyer::all();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function show($id) {

        try{
            $this->result['data'] = Buyer::findOrFail($id);
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function getFavorites($id) {
        try{
            $this->result['data'] = Buyer::with('favorite.product.category', 'favorite.product.file')->has('favorite')->findOrFail($id);
            $this->result['statusCode'] = 200;

        }catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

        }

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'BuyFname' => 'required',
            'BuyMI' => 'required',
            'BuyLname' => 'required',
            'BuyAge' => 'required|numeric',
            'BuyGender' => 'required',
            'BuyConNum' => 'required',
            'BuyOccu' => 'required',
            'BuyAdd' => 'required',
            'BuyDoB' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = Buyer::create($request->all());
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function update(Request $request, $id) {

        try{
            $buyer = Buyer::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $validator = Validator::make($request->all(), [
            'BuyFname' => 'required',
            'BuyMI' => 'required',
            'BuyLname' => 'required',
            'BuyAge' => 'required|numeric',
            'BuyGender' => 'required',
            'BuyOccu' => 'required',
            'BuyAdd' => 'required',
            'BuyDoB' => 'required',
        ]);

        if ($validator->fails()) {
            $this->result['error'] = $validator->errors();
            $this->result['statusCode'] = 401;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $input = $request->all();

        $this->result['data'] = $buyer->update($input);
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }

    public function delete(Request $request, $id) {

        try{
            $buyer = Buyer::findOrFail($id);

        } catch (ModelNotFoundException $exception){
            $this->result['error'] = $exception->getMessage();
            $this->result['statusCode'] = 500;

            return response()->json($this->result, $this->result['statusCode']);
        }

        $this->result['data'] = $buyer->delete();
        $this->result['statusCode'] = 200;

        return response()->json($this->result, $this->result['statusCode']);
    }
}
