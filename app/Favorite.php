<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    //
    protected $fillable = ['BUYERID', 'ProdID'];

    public function buyer() {

        return $this->belongsTo('App\Buyer', 'BUYERID', 'BUYERID');
    }

    public function product() {

        return $this->belongsTo('App\Product', 'ProdID', 'ProdID');
    }
}
