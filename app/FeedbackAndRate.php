<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackAndRate extends Model
{
    //
    protected $primaryKey = 'FEEDRATEID';
    protected $fillable = ['ORDERRATE', 'ORDERFEEDBACKS'];

    public function orderMasterlist() {

        return $this->belongsTo('App\OrderMasterlist', 'OrMasID' , 'OrMasID');
    }
}
