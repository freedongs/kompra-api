<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTrackingStatus extends Model
{
    //
    protected $primaryKey = 'OrTracID';
    protected $fillable = ['OrderStatus', 'OrderTrackDate', 'OrMasID'];

    public function orderMasterlist() {

        return $this->belongsTo('App\OrderMasterlist', 'OrMasID', 'OrMasID');
    }
}
