<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverDetail extends Model
{
    //
    protected $primaryKey = 'DriverID';
    protected $fillable = ['DriverID', 'DriverLicenseNum', 'EMPID'];

    public function orderMasterlist() {

        return $this->belongsTo('App\OrderMasterlist', 'DriverID', 'DriverID');
    }

    public function employee() {

        return $this->belongsTo('App\Employee', 'EMPID', 'EMPID');
    }
}
