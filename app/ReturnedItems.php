<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ReturnedItems extends Model
{
    use HasApiTokens, Notifiable;

        //

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $primaryKey = 'ReturnID';
        protected $fillable = ['ProdIDs', 'ProdQty', 'id', 'received'];
}
