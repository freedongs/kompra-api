<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected $fillable = ['image_url', 'size'];

    public function product() {

        return $this->belongsTo('App\Product', 'ProdPhoto', 'id');
    }

    public function category() {

        return $this->belongsTo('App\Category', 'categoryPhoto', 'id');
    }
}
