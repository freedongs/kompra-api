<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Suppliers extends Model
{
    //
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'supplier_id';
    protected $fillable = ['supplier_name', 'supplier_address', 'supplier_contact',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['remember_token'
    ];

    public function products() {

        return $this->belongsTo('App\Product', 'ProdID', 'ProdID');
    }
}
